# TABLE OF CONTENT
### [[README.md#^c40919 | INTRODUCTION]]
### [[README.md#^f536c6 | INSTALLATION]]
### [[README.md#^917ea6| CONFIGURATION]]
### [[README.md#^3892c3 | REFERENCE]]

# INTRODUCTION
**Egg Defense** is a single player python game with levels and different game modes to play with. Take a look what does it look like, you might like it!

### Egg Defense
Place your heroes to withstand the invasion of monsters. You win when the none of the monsters cross the fence.
![[螢幕錄影 2023-07-30 下午4.23.54.gif | Egg Defense]]

### Main page
Where you navigate the game.
![[Egg_Defense.png]]

### Gesture Control
Having fun playing the game? Gesture Control makes it even more playable, you can pinch to grab, open fingers to place the hero and move around the cursor.

> tips: Switch between gesture control and mouse control in the setting page, it'll remember the setting of last time you playing it.

![[Egg_Defense (1).png]]

### Data Base
**Egg Defense** will remember your previous progress and preferred settings, all thanks to the game's Database.
![[Pasted image 20230730165408.png]]
^c40919

# INSTALLATION
//what packages users have to download
^f536c6

# CONFIGURATION 
>In **cam_selection.py** you can define variable **selected_cam** to 0-2 to your need.
^917ea6


# REFERENCE

^3892c3

