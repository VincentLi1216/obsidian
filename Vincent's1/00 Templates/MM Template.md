---
type: meeting
name: <%tp.frontmatter.parent%>@<%tp.date.now("YYYY-MM-DD")%>
date: <% tp.date.now("YYYY-MM-DD") %>
parent: 一般幹部會議
outline: |
 - 
chairpeople: |
 - 李善得
 - 卓毓謙
attendees: |
 - 所有人
 - 李善得
 - 卓毓謙
 - 周卉琳
 - 劉冠妤
 - 許雅筑
 - 許芷寧
 - 詹維昕
 - 廖浚有
 - 葉雨涵
 - 陳家豪
 - 何老
---
<% await tp.file.move("/03 集美/b. 會議記錄/" + tp.file.title) %>
<% tp.file.rename(tp.frontmatter.parent + "@" + tp.date.now("YYYY-MM-DD")) %>
# Meeting Title
<%tp.frontmatter.parent%>@<%tp.date.now("YYYY-MM-DD")%>


# Date
<% tp.date.now("YYYY-MM-DD") %>


# Chairpeople
<%tp.frontmatter.chairpeople%>

# Attendees
<%tp.frontmatter.attendees%>

# Agenda
1. 


# Next Agenda
1.