---
type: todo
name: <% tp.file.title %>@<%tp.date.now("YY-MM-DD ddd")%>
state: todo
date_created: <% tp.date.now("YYYY-MM-DD") %>
parent: 
due_date: 
assign_to: |
 - 所有人
 - 李善得
 - 卓毓謙
 - 周卉琳
 - 劉冠妤
 - 許雅筑
 - 許芷寧
 - 詹維昕
 - 廖浚有
 - 何老
---
<% await tp.file.move("/03 集美/d. To-Dos/" + tp.file.title) %>
# <%tp.frontmatter.parent%>: <%tp.file.title%>

## Date Created
<% tp.date.now("YYYY-MM-DD ddd") %>


## Assign to
<% tp.frontmatter.assign_to %>

## Dead Line
<% tp.frontmatter.due_date %>


## Link
[[ | <% tp.file.title %>]]


## Note & Solution