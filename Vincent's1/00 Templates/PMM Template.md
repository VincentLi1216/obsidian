---
type: meeting
name: <%tp.frontmatter.parent%>@<%tp.date.now("YYYY-MM-DD")%>
date: <% tp.date.now("YYYY-MM-DD") %>
parent: 會前會
outline: |
 - 
chairpeople: |
 - 李善得
attendees: |
 - 李善得
 - 卓毓謙
---
<% await tp.file.move("/03 集美/c. 會前會議記錄/" + tp.file.title) %>
<% tp.file.rename(tp.frontmatter.parent + "@" + tp.date.now("YYYY-MM-DD")) %>
# Meeting Title
<%tp.frontmatter.parent%>@<%tp.date.now("YYYY-MM-DD")%>


# Date
<% tp.date.now("YYYY-MM-DD") %>


# Chairpeople
<%tp.frontmatter.chairpeople%>

# Attendees
<%tp.frontmatter.attendees%>

# Next Meeting Date



# Agenda
1. 討論下次議程


# Next Agenda
1. 