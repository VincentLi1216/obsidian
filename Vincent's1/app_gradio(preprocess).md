```python
import sys
import io
import requests
import json
import base64
from PIL import Image
import cv2
import numpy as np
import gradio as gr
from pathlib import Path
import os
from SegGPT_inference.seggpt_inference import prepare_model
from SegGPT_inference.seggpt_engine import inference_image_pil
import time
import datetime

sys.path.append('.')


def inference_mask1_sam(prompt,
              img,
              img_):

    files = {
        "useSam" : 1,
        "pimage" : resizeImg(prompt["image"]),
        "pmask" : resizeImg(prompt["mask"]),
        "img" : resizeImg(img),
        "img_" : resizeImg(img_)
    }
    r = requests.post("http://120.92.79.209/painter/run", json = files)
    a = json.loads(r.text)

    res = []

    for i in range(len(a)):
        #out = Image.open(io.BytesIO(base64.b64decode(a[i])))
        #out = out.resize((224, 224))
        #res.append(np.uint8(np.array(out)))
        res.append(np.uint8(np.array(Image.open(io.BytesIO(base64.b64decode(a[i]))))))

    return res[1:] # remove the prompt image

'''
def inference_mask1_pil(prompt1, prompt2, prompt3,
              img,
              img_):

    #prompt: include prompt image and mask
    #img: test image
    #if use only one prompt should be pass.

    #     output = inference_image_pil(model, device, resizeImg(img), resizeImg(prompt["image"]), resizeImg(prompt["mask"]))
    prompt_image_list = []
    prompt_tgt_list = []

    if prompt1:
        prompt_image_list.append(resizeImg(prompt1["image"]))
        prompt_tgt_list.append(resizeImg(prompt1["mask"]))
    if prompt2:
        prompt_image_list.append(resizeImg(prompt2["image"]))
        prompt_tgt_list.append(resizeImg(prompt2["mask"]))
    if prompt3:
        prompt_image_list.append(resizeImg(prompt3["image"]))
        prompt_tgt_list.append(resizeImg(prompt3["mask"]))

    output = inference_image_pil(model, device, resizeImg(img), prompt_image_list, prompt_tgt_list)
    return output
'''
#------------------------------trying to make input as dir-------------------------------------------
def preprocess(img=None, img_path=None):

    def convert_cv2_to_pil(img, grayscale=False):
        if not grayscale:
            # 將 BGR 轉換為 RGB
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)



        # 將 numpy 陣列轉換為 PIL.Image.Image
        img = Image.fromarray(img)

        return img

    def convert_pil_to_cv2(img, grayscale = False):
        # 將 PIL.Image.Image 轉換為 numpy 陣列
        img = np.array(img)

        if not grayscale:

            # 將 RGB 轉換為 BGR
            img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)

        return img


    if img!=None:
        img = convert_pil_to_cv2(img=img, grayscale=True)
    elif img_path!=None:
        img = cv2.imread(img_path)
    else:
        print("preprocess error: either img or img_path")
        return
    print(f"\n\n{type(img_path)}\n\n")
    print(img_path)

    contrast = 50
    brightness = 0
    output = img * (contrast/127 + 1) - contrast + brightness # 轉換公式
    # 轉換公式參考 https://stackoverflow.com/questions/50474302/how-do-i-adjust-brightness-contrast-and-vibrance-with-opencv-python

    # 調整後的數值大多為浮點數，且可能會小於 0 或大於 255
    # 為了保持像素色彩區間為 0～255 的整數，所以再使用 np.clip() 和 np.uint8() 進行轉換
    output = np.clip(output, 0, 255)
    output = np.uint8(output)

    output = cv2.medianBlur(output,5)

    # 全部設為255
    output[output > 50] = 255

    erode_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (4, 4))
    dilate_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (4, 4))

    output = cv2.erode(output, erode_kernel)     # 先侵蝕，將白色小圓點移除

    output = cv2.dilate(output, dilate_kernel)    # 再膨脹，白色小點消失


    output = cv2.medianBlur(output,7)

    # 全部設為255
    output[output > 0] = 255

    # output = convert_cv2_to_pil(output, grayscale=True)

    return output

# +
def inference_mask1_pil(predict_path, prompt1, prompt2, prompt3, prompt4, prompt5, prompt6,prompt7, prompt8, prompt9,prompt10,
              img,
              img_):
    '''
    prompt: include prompt image and mask
    img: test image
    if use only one prompt should be pass.
    '''

    prompt_image_list = []
    prompt_tgt_list = []

    if prompt1:
        # img = resizeImg(prompt1["image"])
        img = preprocess(img)
        prompt_image_list.append(img)
        prompt_tgt_list.append(resizeImg(prompt1["mask"]))
    if prompt2:
        # img = resizeImg(prompt2["image"])
        img = preprocess(img)
        prompt_image_list.append(img)
        prompt_tgt_list.append(resizeImg(prompt2["mask"]))
    if prompt3:
        # img = resizeImg(prompt3["image"])
        img = preprocess(img)
        prompt_image_list.append(img)
        prompt_tgt_list.append(resizeImg(prompt3["mask"]))
    if prompt4:
        # img =resizeImg(prompt4["image"])
        img = preprocess(img)
        prompt_image_list.append(img)
        prompt_tgt_list.append(resizeImg(prompt4["mask"]))
    if prompt5:
        # img = resizeImg(prompt5["image"])
        img = preprocess(img)
        prompt_image_list.append(img)
        prompt_tgt_list.append(resizeImg(prompt5["mask"]))
    if prompt6:
        # img = resizeImg(prompt6["image"])
        img = preprocess(img)
        prompt_image_list.append(img)
        prompt_tgt_list.append(resizeImg(prompt6["mask"]))
    if prompt7:
        # img = resizeImg(prompt7["image"])
        img = preprocess(img)
        prompt_image_list.append(img)
        prompt_tgt_list.append(resizeImg(prompt7["mask"]))
    if prompt8:
        # img = resizeImg(prompt8["image"])
        img = preprocess(img)
        prompt_image_list.append(img)
        prompt_tgt_list.append(resizeImg(prompt8["mask"]))
    if prompt9:
        # img = resizeImg(prompt9["image"])
        img = preprocess(img)
        prompt_image_list.append(img)
        prompt_tgt_list.append(resizeImg(prompt9["mask"]))
    if prompt10:
        # img = resizeImg(prompt10["image"])
        img = preprocess(img)
        prompt_image_list.append(img)
        prompt_tgt_list.append(resizeImg(prompt10["mask"]))


    directory_path = predict_path


# new changes (Predict Path)================================================================================
    # Create the directory

    def get_output_dir(pre_output_dir=""):
        from datetime import datetime, timedelta
        import os
        print("\n\n-------GET OUTPUT DIR-------")
        # 獲得當地日期和時間
        now_datetime = datetime.now()
        local_datetime = now_datetime + timedelta(hours=8)

        # 輸出格式為 "YYYY-MM-DD"
        print(local_datetime.strftime("%Y-%m-%d"))

        # 輸出格式為 "HH:MM:SS"
        print(local_datetime.strftime("%H:%M:%S"))
        output_dir = local_datetime.strftime("%Y-%m-%d_%H:%M:%S")
        output_dir = os.path.join(pre_output_dir, output_dir)
        print(f"output_dir: \"{output_dir}\"")

        if os.path.exists(output_dir):
            return output_dir
        else:
            os.mkdir(output_dir)
            return output_dir

    Predict_Path = get_output_dir("pred_output")


    def sorted_pngs(dir):
        import os
        files = []
        only_file_name = []
        for file in os.listdir(dir):
            if file.endswith(".png"):
                files.append(file)
                # 獲得文件名
                base_name = os.path.basename(file)

                # 去除擴展名
                only_file_name.append(int(os.path.splitext(base_name)[0]))
        only_file_name.sort()
        for i in range(len(only_file_name)):
            files[i] = f"{only_file_name[i]}.png"
        print("\n\n-------DIRECTORY-------")
        print(f"Dir_path: {dir}")
        print(files)
        return files


    start_time = time.time()
    files = sorted_pngs(directory_path)
    for filename in files:
        print(filename)
        img = Image.open(os.path.join(directory_path, filename))
        # img = preprocess(img_path=os.path.join(directory_path, filename))
        print(f"before prediction")
        print(f"type of the img: {type(img)}")
        output = inference_image_pil(model, device, Predict_Path, filename,resizeImg(img), prompt_image_list, prompt_tgt_list)

    end_time = time.time()
    total_time = end_time - start_time
    print("total_time = ",total_time)

    return output

# ------------------------------trying to make input as dir-------------------------------------------
# + +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



def inference_mask1(prompt,
              img,
              img_):
    files = {
        "pimage" : resizeImg(prompt["image"]),
        "pmask" : resizeImg(prompt["mask"]),
        "img" : resizeImg(img),
        "img_" : resizeImg(img_)
    }
    r = requests.post("http://120.92.79.209/painter/run", json = files)
    a = json.loads(r.text)
    res = []
    for i in range(len(a)):
        #out = Image.open(io.BytesIO(base64.b64decode(a[i])))
        #out = out.resize((224, 224))
        #res.append(np.uint8(np.array(out)))
        res.append(np.uint8(np.array(Image.open(io.BytesIO(base64.b64decode(a[i]))))))
    return res


def inference_mask_video(
              prompt,
              vid,
              request: gr.Request,
              ):


    files = {
        "pimage" : resizeImgIo(prompt["image"]),
        "pmask" : resizeImgIo(prompt["mask"]),
        "video" : open(vid, 'rb'),
    }
    r = requests.post("http://120.92.79.209/painter/runVideo", files = files)
    '''
    path = str(uuid.uuid4()) + "." + str(time.time())
    fName = 'out.mp4'
    file_out = "video/" + path + "." + fName
    with open(file_out,"wb") as f:
        f.write(r.content)
    '''
    a = json.loads(r.text)
    return [np.uint8(np.array(Image.open(io.BytesIO(base64.b64decode(a["mask"]))))), a["url"]]


def resizeImg_(img):
    res, hres = 448, 448
    img = Image.fromarray(img).convert("RGB")
    img = img.resize((res, hres))
    temp = io.BytesIO()
    img.save(temp, format="WEBP")
    return base64.b64encode(temp.getvalue()).decode('ascii')

def resizeImg(img):
    res, hres = 448, 448

    #img = Image.fromarray(img).convert("RGB")
    #img = img.resize((res, hres))
    #return img

    img_copy = np.array(img, copy=True)
    img_pil = Image.fromarray(img_copy).convert("RGB")
    img_resized = img_pil.resize((res, hres))
    return img_resized


def resizeImgIo(img):
    res, hres = 448, 448
    img = Image.fromarray(img).convert("RGB")
    img = img.resize((res, hres))
    temp = io.BytesIO()
    img.save(temp, format="WEBP")
    return io.BytesIO(temp.getvalue())


# define app features and run

examples = [
            ['./images/hmbb_1.jpg', './images/hmbb_2.jpg', './images/hmbb_3.jpg'],
            ['./images/rainbow_1.jpg', './images/rainbow_2.jpg', './images/rainbow_3.jpg'],
            ['./images/earth_1.jpg', './images/earth_2.jpg', './images/earth_3.jpg'],
            ['./images/obj_1.jpg', './images/obj_2.jpg', './images/obj_3.jpg'],
            ['./images/ydt_2.jpg', './images/ydt_1.jpg', './images/ydt_3.jpg'],
           ]

examples_sam = [
            ['./images/hmbb_1.jpg', './images/hmbb_2.jpg', './images/hmbb_3.jpg'],
            ['./images/street_1.jpg', './images/street_2.jpg', './images/street_3.jpg'],
            ['./images/tom_1.jpg', './images/tom_2.jpg', './images/tom_3.jpg'],
            ['./images/earth_1.jpg', './images/earth_2.jpg', './images/earth_3.jpg'],
            ['./images/ydt_2.jpg', './images/ydt_1.jpg', './images/ydt_3.jpg'],
           ]

examples_pil = [
            ['./images/hmbb_1.jpg', './images/hmbb_2.jpg', './images/hmbb_3.jpg', './images/hmbb_4.jpg'],
            ['./images/rainbow_1.jpg', './images/rainbow_2.jpg', './images/rainbow_3.jpg', './images/rainbow_4.jpg'],
            ['./images/earth_1.jpg', './images/earth_2.jpg', './images/earth_3.jpg', './images/earth_4.jpg'],
            ['./images/obj_1.jpg', './images/obj_2.jpg', './images/obj_3.jpg', './images/obj_4.jpg'],
            ['./images/ydt_2.jpg', './images/ydt_1.jpg', './images/ydt_3.jpg', './images/ydt_4.jpg'],
           ]

examples_video = [
            ['./videos/horse-running.jpg', './videos/horse-running.mp4'],
            ['./videos/a_man_is_surfing_3_30.jpg', './videos/a_man_is_surfing_3_30.mp4'],
    ['./videos/a_car_is_moving_on_the_road_40.jpg', './videos/a_car_is_moving_on_the_road_40.mp4'],
['./videos/jeep-moving.jpg', './videos/jeep-moving.mp4'],
['./videos/child-riding_lego.jpg', './videos/child-riding_lego.mp4'],
['./videos/a_man_in_parkour_100.jpg', './videos/a_man_in_parkour_100.mp4'],
]

# ===============================================================================================================

device = "cpu"
# ===============================================================================================================


model = prepare_model("SegGPT_inference/seggpt_vit_large.pth", "seggpt_vit_large_patch16_input896x448", "instance").to(device)
print('Model loaded.')


demo_mask = gr.Interface(fn=inference_mask1,
                   inputs=[gr.ImageMask(brush_radius=8, label="prompt (提示图)"), gr.Image(label="img1 (测试图1)"), gr.Image(label="img2 (测试图2)")],
                    #outputs=[gr.Image(shape=(448, 448), label="output1 (输出图1)"), gr.Image(shape=(448, 448), label="output2 (输出图2)")],
                    outputs=[gr.Image(label="output1 (输出图1)").style(height=256, width=256), gr.Image(label="output2 (输出图2)").style(height=256, width=256)],
                    #outputs=gr.Gallery(label="outputs (输出图)"),
                    examples=examples,
                    #title="SegGPT for Any Segmentation<br>(Painter Inside)",
                    description="<p> \
                    Choose an example below &#128293; &#128293;  &#128293; <br>\
                    Or, upload by yourself: <br>\
                    1. Upload images to be tested to 'img1' and/or 'img2'. <br>2. Upload a prompt image to 'prompt' and draw a mask.  <br>\
                            <br> \
                            💎 The more accurate you annotate, the more accurate the model predicts. <br>\
                            💎 Examples below were never trained and are randomly selected for testing in the wild. <br>\
                            💎 Current UI interface only unleashes a small part of the capabilities of SegGPT, i.e., 1-shot case. \
</p>",
                   cache_examples=False,
                   allow_flagging="never",
                   )

demo_mask_pil = gr.Interface(fn=inference_mask1_pil,
                   inputs=[
                           gr.Textbox(label="Predict Path"),
                           gr.ImageMask(brush_radius=8, label="prompt1"),
                           gr.ImageMask(brush_radius=8, label="prompt2"),
                           gr.ImageMask(brush_radius=8, label="prompt3"),
                           gr.ImageMask(brush_radius=8, label="prompt4"),
                           gr.ImageMask(brush_radius=8, label="prompt5"),
                           gr.ImageMask(brush_radius=8, label="prompt6"),
                           gr.ImageMask(brush_radius=8, label="prompt7"),
                           gr.ImageMask(brush_radius=8, label="prompt8"),
                           gr.ImageMask(brush_radius=8, label="prompt9"),
                           gr.ImageMask(brush_radius=8, label="prompt10"),

                           ],
                    #outputs=[gr.Image(shape=(448, 448), label="output1 (输出图1)"), gr.Image(shape=(448, 448), label="output2 (输出图2)")],
                    outputs=[gr.Image(label="output1 (输出图1)").style(height=256, width=256)],
                    #outputs=gr.Gallery(label="outputs (输出图)"),
                    examples=examples_pil,
                    #title="SegGPT for Any Segmentation<br>(Painter Inside)",
                    description="<p> \
                    Choose an example below &#128293; &#128293;  &#128293; <br>\
                    Or, upload by yourself: <br>\
                    1. Upload images to be tested to 'img1' and/or 'img2'. <br>2. Upload a prompt image to 'prompt' and draw a mask.  <br>\
                            <br> \
                            💎 The more accurate you annotate, the more accurate the model predicts. <br>\
                            💎 Examples below were never trained and are randomly selected for testing in the wild. <br>\
                            💎 Current UI interface only unleashes a small part of the capabilities of SegGPT, i.e., 1-shot case. \
</p>",
                   cache_examples=False,
                   allow_flagging="never",
                   )


demo_mask_sam = gr.Interface(fn=inference_mask1_sam,
                   inputs=[gr.ImageMask(brush_radius=4, label="prompt (提示图)"), gr.Image(label="img1 (测试图1)"), gr.Image(label="img2 (测试图2)")],
                    #outputs=[gr.Image(shape=(448, 448), label="output1 (输出图1)"), gr.Image(shape=(448, 448), label="output2 (输出图2)")],
                    # outputs=[gr.Image(label="output1 (输出图1)").style(height=256, width=256), gr.Image(label="output2 (输出图2)").style(height=256, width=256)],
                    #outputs=gr.Gallery(label="outputs (输出图)"),
                    outputs=[gr.Image(label="SAM output (mask)").style(height=256, width=256),gr.Image(label="output1 (输出图1)").style(height=256, width=256), gr.Image(label="output2 (输出图2)").style(height=256, width=256)],
                    # outputs=[gr.Image(label="output3 (输出图1)").style(height=256, width=256), gr.Image(label="output4 (输出图2)").style(height=256, width=256)],
                    examples=examples_sam,
                    #title="SegGPT for Any Segmentation<br>(Painter Inside)",
                    description="<p> \
                    <strong>SAM+SegGPT: One touch for segmentation in all images or videos.</strong> <br>\
                    Choose an example below &#128293; &#128293;  &#128293; <br>\
                    Or, upload by yourself: <br>\
                    1. Upload images to be tested to 'img1' and 'img2'. <br>2. Upload a prompt image to 'prompt' and draw <strong>a point or line on the target</strong>.  <br>\
                            <br> \
                            💎 SAM segments the target with any point or scribble, then SegGPT segments all other images. <br>\
                            💎 Examples below were never trained and are randomly selected for testing in the wild. <br>\
                            💎 Current UI interface only unleashes a small part of the capabilities of SegGPT, i.e., 1-shot case. \
</p>",
                   cache_examples=False,
                   allow_flagging="never",
                   )

demo_mask_video = gr.Interface(fn=inference_mask_video,
                   inputs=[gr.ImageMask(label="prompt (提示图)"), gr.Video(label="video (测试视频)").style(height=448, width=448)],
                    outputs=[gr.Image(label="SAM output (mask)").style(height=256, width=256), gr.Video().style(height=448, width=448)],
                    examples=examples_video,
                    description="<p> \
                    <strong>SegGPT+SAM: One touch for any segmentation in a video.</strong> <br>\
                    Choose an example below &#128293; &#128293;  &#128293; <br>\
                    Or, upload by yourself: <br>\
                    1. Upload a video to be tested to 'video'. If failed, please check the codec, we recommend h.264 by default. <br>2. Upload a prompt image to 'prompt' and draw <strong>a point or line on the target</strong>.  <br>\
<br> \
💎 SAM segments the target with any point or scribble, then SegGPT segments the whole video. <br>\
💎 Examples below were never trained and are randomly selected for testing in the wild. <br>\
💎 Current UI interface only unleashes a small part of the capabilities of SegGPT, i.e., 1-shot case. <br> \
                Note: we only take the first 16 frames for the demo.    \
</p>",
                   )



title = "SegGPT: Segmenting Everything In Context<br> \
<div align='center'> \
<h2><a href='https://arxiv.org/abs/2304.03284' target='_blank' rel='noopener'>[paper]</a> \
<a href='https://github.com/baaivision/Painter' target='_blank' rel='noopener'>[code]</a></h2> \
<br> \
<image src='file/rainbow.gif' width='720px' /> \
<h2>SegGPT performs arbitrary segmentation tasks in images or videos via in-context inference, such as object instance, stuff, part, contour, and text, with only one single model.</h2> \
</div> \
"

#demo = gr.TabbedInterface([demo_mask_sam, demo_mask_video, demo_mask], ['SAM+SegGPT (一触百通)', '🎬Anything in a Video', 'General 1-shot'], title=title)
demo = gr.TabbedInterface([demo_mask_pil], ['DEMO'], title=title)


#demo.launch(share=True, auth=("baai", "vision"))
# demo.launch(enable_queue=False)
#demo.launch(enable_queue=True, server_name="0.0.0.0")
demo.launch(enable_queue=False, server_name="0.0.0.0", server_port=6006)
```

# Working commit
```python
import sys
import io
import requests
import json
import base64
from PIL import Image
import numpy as np
import gradio as gr
from pathlib import Path
import os
from SegGPT_inference.seggpt_inference import prepare_model
from SegGPT_inference.seggpt_engine import inference_image_pil
import time
import datetime

sys.path.append('.')


def inference_mask1_sam(prompt,
              img,
              img_):

    files = {
        "useSam" : 1,
        "pimage" : resizeImg(prompt["image"]),
        "pmask" : resizeImg(prompt["mask"]),
        "img" : resizeImg(img),
        "img_" : resizeImg(img_)
    }
    r = requests.post("http://120.92.79.209/painter/run", json = files)
    a = json.loads(r.text)

    res = []

    for i in range(len(a)):
        #out = Image.open(io.BytesIO(base64.b64decode(a[i])))
        #out = out.resize((224, 224))
        #res.append(np.uint8(np.array(out)))
        res.append(np.uint8(np.array(Image.open(io.BytesIO(base64.b64decode(a[i]))))))

    return res[1:] # remove the prompt image

'''
def inference_mask1_pil(prompt1, prompt2, prompt3,
              img,
              img_):

    #prompt: include prompt image and mask
    #img: test image
    #if use only one prompt should be pass.

    #     output = inference_image_pil(model, device, resizeImg(img), resizeImg(prompt["image"]), resizeImg(prompt["mask"]))
    prompt_image_list = []
    prompt_tgt_list = []

    if prompt1:
        prompt_image_list.append(resizeImg(prompt1["image"]))
        prompt_tgt_list.append(resizeImg(prompt1["mask"]))
    if prompt2:
        prompt_image_list.append(resizeImg(prompt2["image"]))
        prompt_tgt_list.append(resizeImg(prompt2["mask"]))
    if prompt3:
        prompt_image_list.append(resizeImg(prompt3["image"]))
        prompt_tgt_list.append(resizeImg(prompt3["mask"]))

    output = inference_image_pil(model, device, resizeImg(img), prompt_image_list, prompt_tgt_list)
    return output
'''
#------------------------------trying to make input as dir-------------------------------------------

# +
def inference_mask1_pil(predict_path, prompt1, prompt2, prompt3, prompt4, prompt5, prompt6,prompt7, prompt8, prompt9,prompt10,
              img,
              img_):
    '''
    prompt: include prompt image and mask
    img: test image
    if use only one prompt should be pass.
    '''

    prompt_image_list = []
    prompt_tgt_list = []

    if prompt1:
        prompt_image_list.append(resizeImg(prompt1["image"]))
        prompt_tgt_list.append(resizeImg(prompt1["mask"]))
    if prompt2:
        prompt_image_list.append(resizeImg(prompt2["image"]))
        prompt_tgt_list.append(resizeImg(prompt2["mask"]))
    if prompt3:
        prompt_image_list.append(resizeImg(prompt3["image"]))
        prompt_tgt_list.append(resizeImg(prompt3["mask"]))
    if prompt4:
        prompt_image_list.append(resizeImg(prompt4["image"]))
        prompt_tgt_list.append(resizeImg(prompt4["mask"]))
    if prompt5:
        prompt_image_list.append(resizeImg(prompt5["image"]))
        prompt_tgt_list.append(resizeImg(prompt5["mask"]))
    if prompt6:
        prompt_image_list.append(resizeImg(prompt6["image"]))
        prompt_tgt_list.append(resizeImg(prompt6["mask"]))
    if prompt7:
        prompt_image_list.append(resizeImg(prompt7["image"]))
        prompt_tgt_list.append(resizeImg(prompt7["mask"]))
    if prompt8:
        prompt_image_list.append(resizeImg(prompt8["image"]))
        prompt_tgt_list.append(resizeImg(prompt8["mask"]))
    if prompt9:
        prompt_image_list.append(resizeImg(prompt9["image"]))
        prompt_tgt_list.append(resizeImg(prompt9["mask"]))
    if prompt10:
        prompt_image_list.append(resizeImg(prompt10["image"]))
        prompt_tgt_list.append(resizeImg(prompt10["mask"]))

    directory_path = predict_path


# new changes (Predict Path)================================================================================
    # Create the directory

    def get_output_dir(pre_output_dir=""):
        from datetime import datetime, timedelta
        import os
        print("\n\n-------GET OUTPUT DIR-------")
        # 獲得當地日期和時間
        now_datetime = datetime.now()
        local_datetime = now_datetime + timedelta(hours=8)

        # 輸出格式為 "YYYY-MM-DD"
        print(local_datetime.strftime("%Y-%m-%d"))

        # 輸出格式為 "HH:MM:SS"
        print(local_datetime.strftime("%H:%M:%S"))
        output_dir = local_datetime.strftime("%Y-%m-%d_%H:%M:%S")
        output_dir = os.path.join(pre_output_dir, output_dir)
        print(f"output_dir: \"{output_dir}\"")

        if os.path.exists(output_dir):
            return output_dir
        else:
            os.mkdir(output_dir)
            return output_dir

    Predict_Path = get_output_dir("pred_output")


    def sorted_pngs(dir):
        import os
        files = []
        only_file_name = []
        for file in os.listdir(dir):
            if file.endswith(".png"):
                files.append(file)
                # 獲得文件名
                base_name = os.path.basename(file)

                # 去除擴展名
                only_file_name.append(int(os.path.splitext(base_name)[0]))
        only_file_name.sort()
        for i in range(len(only_file_name)):
            files[i] = f"{only_file_name[i]}.png"
        print("\n\n-------DIRECTORY-------")
        print(f"Dir_path: {dir}")
        print(files)
        return files

    start_time = time.time()
    files = sorted_pngs(directory_path)
    for filename in files:
        print(filename)
        img = Image.open(os.path.join(directory_path, filename))
        output = inference_image_pil(model, device, Predict_Path, filename,resizeImg(img), prompt_image_list, prompt_tgt_list)

    end_time = time.time()
    total_time = end_time - start_time
    print("total_time = ",total_time)

    return output

# ------------------------------trying to make input as dir-------------------------------------------
# + +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



def inference_mask1(prompt,
              img,
              img_):
    files = {
        "pimage" : resizeImg(prompt["image"]),
        "pmask" : resizeImg(prompt["mask"]),
        "img" : resizeImg(img),
        "img_" : resizeImg(img_)
    }
    r = requests.post("http://120.92.79.209/painter/run", json = files)
    a = json.loads(r.text)
    res = []
    for i in range(len(a)):
        #out = Image.open(io.BytesIO(base64.b64decode(a[i])))
        #out = out.resize((224, 224))
        #res.append(np.uint8(np.array(out)))
        res.append(np.uint8(np.array(Image.open(io.BytesIO(base64.b64decode(a[i]))))))
    return res


def inference_mask_video(
              prompt,
              vid,
              request: gr.Request,
              ):


    files = {
        "pimage" : resizeImgIo(prompt["image"]),
        "pmask" : resizeImgIo(prompt["mask"]),
        "video" : open(vid, 'rb'),
    }
    r = requests.post("http://120.92.79.209/painter/runVideo", files = files)
    '''
    path = str(uuid.uuid4()) + "." + str(time.time())
    fName = 'out.mp4'
    file_out = "video/" + path + "." + fName
    with open(file_out,"wb") as f:
        f.write(r.content)
    '''
    a = json.loads(r.text)
    return [np.uint8(np.array(Image.open(io.BytesIO(base64.b64decode(a["mask"]))))), a["url"]]


def resizeImg_(img):
    res, hres = 448, 448
    img = Image.fromarray(img).convert("RGB")
    img = img.resize((res, hres))
    temp = io.BytesIO()
    img.save(temp, format="WEBP")
    return base64.b64encode(temp.getvalue()).decode('ascii')

def resizeImg(img):
    res, hres = 448, 448

    #img = Image.fromarray(img).convert("RGB")
    #img = img.resize((res, hres))
    #return img

    img_copy = np.array(img, copy=True)
    img_pil = Image.fromarray(img_copy).convert("RGB")
    img_resized = img_pil.resize((res, hres))
    return img_resized


def resizeImgIo(img):
    res, hres = 448, 448
    img = Image.fromarray(img).convert("RGB")
    img = img.resize((res, hres))
    temp = io.BytesIO()
    img.save(temp, format="WEBP")
    return io.BytesIO(temp.getvalue())


# define app features and run

examples = [
            ['./images/hmbb_1.jpg', './images/hmbb_2.jpg', './images/hmbb_3.jpg'],
            ['./images/rainbow_1.jpg', './images/rainbow_2.jpg', './images/rainbow_3.jpg'],
            ['./images/earth_1.jpg', './images/earth_2.jpg', './images/earth_3.jpg'],
            ['./images/obj_1.jpg', './images/obj_2.jpg', './images/obj_3.jpg'],
            ['./images/ydt_2.jpg', './images/ydt_1.jpg', './images/ydt_3.jpg'],
           ]

examples_sam = [
            ['./images/hmbb_1.jpg', './images/hmbb_2.jpg', './images/hmbb_3.jpg'],
            ['./images/street_1.jpg', './images/street_2.jpg', './images/street_3.jpg'],
            ['./images/tom_1.jpg', './images/tom_2.jpg', './images/tom_3.jpg'],
            ['./images/earth_1.jpg', './images/earth_2.jpg', './images/earth_3.jpg'],
            ['./images/ydt_2.jpg', './images/ydt_1.jpg', './images/ydt_3.jpg'],
           ]

examples_pil = [
            ['./images/hmbb_1.jpg', './images/hmbb_2.jpg', './images/hmbb_3.jpg', './images/hmbb_4.jpg'],
            ['./images/rainbow_1.jpg', './images/rainbow_2.jpg', './images/rainbow_3.jpg', './images/rainbow_4.jpg'],
            ['./images/earth_1.jpg', './images/earth_2.jpg', './images/earth_3.jpg', './images/earth_4.jpg'],
            ['./images/obj_1.jpg', './images/obj_2.jpg', './images/obj_3.jpg', './images/obj_4.jpg'],
            ['./images/ydt_2.jpg', './images/ydt_1.jpg', './images/ydt_3.jpg', './images/ydt_4.jpg'],
           ]

examples_video = [
            ['./videos/horse-running.jpg', './videos/horse-running.mp4'],
            ['./videos/a_man_is_surfing_3_30.jpg', './videos/a_man_is_surfing_3_30.mp4'],
    ['./videos/a_car_is_moving_on_the_road_40.jpg', './videos/a_car_is_moving_on_the_road_40.mp4'],
['./videos/jeep-moving.jpg', './videos/jeep-moving.mp4'],
['./videos/child-riding_lego.jpg', './videos/child-riding_lego.mp4'],
['./videos/a_man_in_parkour_100.jpg', './videos/a_man_in_parkour_100.mp4'],
]

# ===============================================================================================================

device = "cuda"
# ===============================================================================================================


model = prepare_model("SegGPT_inference/seggpt_vit_large.pth", "seggpt_vit_large_patch16_input896x448", "instance").to(device)
print('Model loaded.')


demo_mask = gr.Interface(fn=inference_mask1,
                   inputs=[gr.ImageMask(brush_radius=8, label="prompt (提示图)"), gr.Image(label="img1 (测试图1)"), gr.Image(label="img2 (测试图2)")],
                    #outputs=[gr.Image(shape=(448, 448), label="output1 (输出图1)"), gr.Image(shape=(448, 448), label="output2 (输出图2)")],
                    outputs=[gr.Image(label="output1 (输出图1)").style(height=256, width=256), gr.Image(label="output2 (输出图2)").style(height=256, width=256)],
                    #outputs=gr.Gallery(label="outputs (输出图)"),
                    examples=examples,
                    #title="SegGPT for Any Segmentation<br>(Painter Inside)",
                    description="<p> \
                    Choose an example below &#128293; &#128293;  &#128293; <br>\
                    Or, upload by yourself: <br>\
                    1. Upload images to be tested to 'img1' and/or 'img2'. <br>2. Upload a prompt image to 'prompt' and draw a mask.  <br>\
                            <br> \
                            💎 The more accurate you annotate, the more accurate the model predicts. <br>\
                            💎 Examples below were never trained and are randomly selected for testing in the wild. <br>\
                            💎 Current UI interface only unleashes a small part of the capabilities of SegGPT, i.e., 1-shot case. \
</p>",
                   cache_examples=False,
                   allow_flagging="never",
                   )

demo_mask_pil = gr.Interface(fn=inference_mask1_pil,
                   inputs=[
                           gr.Textbox(label="Predict Path"),
                           gr.ImageMask(brush_radius=8, label="prompt1 (提示图)"),
                           gr.ImageMask(brush_radius=8, label="prompt2 (提示图)"),
                           gr.ImageMask(brush_radius=8, label="prompt3 (提示图)"),
                           gr.ImageMask(brush_radius=8, label="prompt1 (提示图)"),
                           gr.ImageMask(brush_radius=8, label="prompt2 (提示图)"),
                           gr.ImageMask(brush_radius=8, label="prompt3 (提示图)"),
                           gr.ImageMask(brush_radius=8, label="prompt1 (提示图)"),
                           gr.ImageMask(brush_radius=8, label="prompt2 (提示图)"),
                           gr.ImageMask(brush_radius=8, label="prompt3 (提示图)"),
                           gr.ImageMask(brush_radius=8, label="prompt1 (提示图)"),

                           ],
                    #outputs=[gr.Image(shape=(448, 448), label="output1 (输出图1)"), gr.Image(shape=(448, 448), label="output2 (输出图2)")],
                    outputs=[gr.Image(label="output1 (输出图1)").style(height=256, width=256)],
                    #outputs=gr.Gallery(label="outputs (输出图)"),
                    examples=examples_pil,
                    #title="SegGPT for Any Segmentation<br>(Painter Inside)",
                    description="<p> \
                    Choose an example below &#128293; &#128293;  &#128293; <br>\
                    Or, upload by yourself: <br>\
                    1. Upload images to be tested to 'img1' and/or 'img2'. <br>2. Upload a prompt image to 'prompt' and draw a mask.  <br>\
                            <br> \
                            💎 The more accurate you annotate, the more accurate the model predicts. <br>\
                            💎 Examples below were never trained and are randomly selected for testing in the wild. <br>\
                            💎 Current UI interface only unleashes a small part of the capabilities of SegGPT, i.e., 1-shot case. \
</p>",
                   cache_examples=False,
                   allow_flagging="never",
                   )


demo_mask_sam = gr.Interface(fn=inference_mask1_sam,
                   inputs=[gr.ImageMask(brush_radius=4, label="prompt (提示图)"), gr.Image(label="img1 (测试图1)"), gr.Image(label="img2 (测试图2)")],
                    #outputs=[gr.Image(shape=(448, 448), label="output1 (输出图1)"), gr.Image(shape=(448, 448), label="output2 (输出图2)")],
                    # outputs=[gr.Image(label="output1 (输出图1)").style(height=256, width=256), gr.Image(label="output2 (输出图2)").style(height=256, width=256)],
                    #outputs=gr.Gallery(label="outputs (输出图)"),
                    outputs=[gr.Image(label="SAM output (mask)").style(height=256, width=256),gr.Image(label="output1 (输出图1)").style(height=256, width=256), gr.Image(label="output2 (输出图2)").style(height=256, width=256)],
                    # outputs=[gr.Image(label="output3 (输出图1)").style(height=256, width=256), gr.Image(label="output4 (输出图2)").style(height=256, width=256)],
                    examples=examples_sam,
                    #title="SegGPT for Any Segmentation<br>(Painter Inside)",
                    description="<p> \
                    <strong>SAM+SegGPT: One touch for segmentation in all images or videos.</strong> <br>\
                    Choose an example below &#128293; &#128293;  &#128293; <br>\
                    Or, upload by yourself: <br>\
                    1. Upload images to be tested to 'img1' and 'img2'. <br>2. Upload a prompt image to 'prompt' and draw <strong>a point or line on the target</strong>.  <br>\
                            <br> \
                            💎 SAM segments the target with any point or scribble, then SegGPT segments all other images. <br>\
                            💎 Examples below were never trained and are randomly selected for testing in the wild. <br>\
                            💎 Current UI interface only unleashes a small part of the capabilities of SegGPT, i.e., 1-shot case. \
</p>",
                   cache_examples=False,
                   allow_flagging="never",
                   )

demo_mask_video = gr.Interface(fn=inference_mask_video,
                   inputs=[gr.ImageMask(label="prompt (提示图)"), gr.Video(label="video (测试视频)").style(height=448, width=448)],
                    outputs=[gr.Image(label="SAM output (mask)").style(height=256, width=256), gr.Video().style(height=448, width=448)],
                    examples=examples_video,
                    description="<p> \
                    <strong>SegGPT+SAM: One touch for any segmentation in a video.</strong> <br>\
                    Choose an example below &#128293; &#128293;  &#128293; <br>\
                    Or, upload by yourself: <br>\
                    1. Upload a video to be tested to 'video'. If failed, please check the codec, we recommend h.264 by default. <br>2. Upload a prompt image to 'prompt' and draw <strong>a point or line on the target</strong>.  <br>\
<br> \
💎 SAM segments the target with any point or scribble, then SegGPT segments the whole video. <br>\
💎 Examples below were never trained and are randomly selected for testing in the wild. <br>\
💎 Current UI interface only unleashes a small part of the capabilities of SegGPT, i.e., 1-shot case. <br> \
                Note: we only take the first 16 frames for the demo.    \
</p>",
                   )



title = "SegGPT: Segmenting Everything In Context<br> \
<div align='center'> \
<h2><a href='https://arxiv.org/abs/2304.03284' target='_blank' rel='noopener'>[paper]</a> \
<a href='https://github.com/baaivision/Painter' target='_blank' rel='noopener'>[code]</a></h2> \
<br> \
<image src='file/rainbow.gif' width='720px' /> \
<h2>SegGPT performs arbitrary segmentation tasks in images or videos via in-context inference, such as object instance, stuff, part, contour, and text, with only one single model.</h2> \
</div> \
"

#demo = gr.TabbedInterface([demo_mask_sam, demo_mask_video, demo_mask], ['SAM+SegGPT (一触百通)', '🎬Anything in a Video', 'General 1-shot'], title=title)
demo = gr.TabbedInterface([demo_mask_pil], ['DEMO'], title=title)


#demo.launch(share=True, auth=("baai", "vision"))
# demo.launch(enable_queue=False)
#demo.launch(enable_queue=True, server_name="0.0.0.0")
demo.launch(enable_queue=False, server_name="0.0.0.0", server_port=6006)
```