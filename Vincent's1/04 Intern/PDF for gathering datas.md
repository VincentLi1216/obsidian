# PDF
## Whole PDF
![[Cambrian-α_competition_2021_SOP_v3 (1).pdf]]

## Steps

### Go to Kaggle.com
[link](kaggle.com)

![[Cambrian-α_competition_2021_SOP_v3 (1).pdf#page=22]]

### Install SDK
```shell
pip3 install -i http://pypiserver.pegatroncorp.com/ cambrian competition --trusted-host pypiserver.pegatroncorp.com -U
```

### Check your SDK version by
```shell
cambrian -h
competition -h
```

![[Cambrian-α_competition_2021_SOP_v3 (1).pdf#page=23]]

### Binding kaggle.json info
```shell
competition kaggle-link
```

![[Kaggle.json#username]]

![[Kaggle.json#key]]

![[Cambrian-α_competition_2021_SOP_v3 (1).pdf#page=24]]

### PROJECT_TOKEN
```
f881afcc944248f5d009a994c766a81b
```

### DATASET_TOKEN
```
ad8166645a24ade62ccac0186743b2ff
```

### Download Dataset
```shell
cambrian dataset get -t f881afcc944248f5d009a994c766a81b -d ad8166645a24ade62ccac0186743b2ff -f competition-default.hdf5
```

```shell
cambrian dataset get -t eb4db48206ceb31c5f5ce6d28494491f -d 6fe778393cf85dc3c76c945336c749a4 -f competition-default.hdf5
```

![[Cambrian-α_competition_2021_SOP_v3 (1).pdf#page=25]]

### .hdf5 -> 原始圖片
```shell
git clone http://git.cambrian.pegatroncorp.com/cambrian/ds-tools.git
```

```shell
# echo "PROJECT_TOKEN,DATASET_TOKEN,FILE_NAME.hdf5">datasetList.txt
echo "f881afcc944248f5d009a994c766a81b,ad8166645a24ade62ccac0186743b2ff,competition-default.hdf5">datasetList.txt
```

```shell
# echo "PROJECT_TOKEN,DATASET_TOKEN,FILE_NAME.hdf5">datasetList.txt
echo "eb4db48206ceb31c5f5ce6d28494491f,6fe778393cf85dc3c76c945336c749a4,competition-default.hdf5">datasetList.txt
```

```shell
python3 dataset_to_image.py --ds_list='datasetList.txt' --out_dir=OUTPUT_DIR --keep_dataset=True
```