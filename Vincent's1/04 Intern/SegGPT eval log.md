# eval log
#### ./pred_output/2023-08-04_12:24:36
5 prompts(training)
Dice:0.11749554941100716
AP:0.3161141826923077
IoU:0.24305316305474173

test_set:0-546
train_set:546-646

#### ./pred_output/2023-08-07_09:29:39
4 prompts
Dice:0.07845978043504877
AP:0.3089109885731995
IoU:0.20043899610004495

### Change data set
**test** originals(397張 )
```
/notebooks/DD_origin/defect-detection/SegGPT/painter-playground/SegGPT/NEW_1-397_test/originals/
```

test answers
```
/notebooks/DD_origin/defect-detection/SegGPT/painter-playground/SegGPT/NEW_1-397_test/answers/
```

**train** originals(74張)
```
/notebooks/DD_origin/defect-detection/SegGPT/painter-playground/SegGPT/NEW_398-471_train/originals/
```

train answers
```
/notebooks/DD_origin/defect-detection/SegGPT/painter-playground/SegGPT/NEW_398-471_train/answers/
```

#### ./pred_output/2023-08-07_10:51:47
```
./pred_output/2023-08-07_10:51:47
```

prompt:
400
410
450
444
time = 1059/397 = 2.66
Dice:0.35677595728796613
AP:0.5977739504151794
IoU:0.340915394634577

### Add small test data set
**small test** originals(40張)
```
/notebooks/DD_origin/defect-detection/SegGPT/painter-playground/SegGPT/NEW_1-40_small_test/originals
```

small test answers
```
/notebooks/DD_origin/defect-detection/SegGPT/painter-playground/SegGPT/NEW_1-40_small_test/answers
```

#### ./pred_output/2023-08-07_11:42:20
promt:
430
470
455
413
444
400
407

OOM

#### ./pred_output/2023-08-07_11:43:40
promt:
430
470
455
413
444
400

OOM

#### ./pred_output/2023-08-07_11:44:23
promt:
430
470
455
413
444

OOM


#### ./pred_output/2023-08-07_11:49:03
4張promt
time = 106/40 = 2.65

Dice:0.27682006330669406
AP:0.6266217134166785
IoU:0.275944409349481


#### ./pred_output/2023-08-07_11:54:13
5張promt
time = 123/40 = 3.075

Dice:0.3552792811120821
AP:0.6717233884141265
IoU:0.33730941551848537

#### ./pred_output/2023-08-07_12:02:06
6張prompt
time = 145/40 = 3.26

Dice:0.3744558461646101
AP:0.6924911595376185
IoU:0.3574286009764204

#### ./pred_output/2023-08-07_12:09:42
6張prompt
**!!!!!使用train set 預測!!!!!!**
time = 258/74 = 3.4

Dice:0.3255292670369891
AP:0.6689689967691622
IoU:0.3252674647238839


# predict很爛的照片
399
400
401

# predict 還ok的照片
23
16

#### ./pred_output/2023-08-07_14:44:29
6張prompt
time = 146/40 = 3.626

Dice:0.35664934641527013
AP:0.6937432981004902
IoU:0.3512238901288963


#### ./pred_output/2023-08-07_14:59:58
6張prompt
找成效還行的照片來prompt3
tim3 = 142/40 = 3.55

Dice:0.32756740557064074
AP:0.665799080431805
IoU:0.3234127112216657

#### ./pred_output/2023-08-07_16:44:24
4張prompt
time = 76/40 = 1.9



#### pred_output/2023-08-08_16:01:43
435
430
465
455

Dice:0.40392714223854453
AP:0.4400645827401012
IoU:0.3813454480462792
total_time =  114.94471025466919/40 = 2.85


#### pred_output/2023-08-08_16:11:13
5張亮色系promt

Dice:0.41248045994924853
AP:0.4447157779203382
IoU:0.38701846915526184
total_time =  128.54014682769775/40 = 3.2

#### pred_output/2023-08-08_16:36:43
自己畫的5張prompts

Dice:0.4664551277544441
AP:0.5296113577610833
IoU:0.41963171368747043
total_time =  132.00856924057007/40 = 3.3

#### pred_output/2023-08-08_16:48:53
自己畫的5張prompts

Dice:0.42980111569841006
AP:0.6592407117563756
IoU:0.38713803070568736

#### ./pred_output/2023-08-09_09:23:33
6張prompts
較粗的標記

Dice:0.4716135628981129
AP:0.5795113255034674
IoU:0.42021685081604954
time = 149.02774024009705/40 = 3.7256935060024263


#### pred_output/2023-08-09_09:31:31
6張超細標記

Dice:0.46434166432526747
AP:0.6111160381144307
IoU:0.39456719824385605
time = 141.65616464614868/40 = 3.541404116153717


#### pred_output/2023-08-09_10:06:12
6張prompt
加了動態調整threshold

Dice:0.41881263538172303
AP:0.512757913239945
IoU:0.3678828317583532
time = 149.52621006965637/40 = 3.7381552517414094

#### pred_output/2023-08-09_09:23:33
自己標記的答案

Dice:0.5904497358822041
AP:0.8356807079596317
IoU:0.5203403052129258

util_dice.py util_eval.py util_iou.py util_map.py
