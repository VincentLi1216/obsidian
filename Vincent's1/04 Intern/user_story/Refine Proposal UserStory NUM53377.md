As a manufacturing department manager, I want to have a executable proposal of the DL project, so that I can let the development team start to implement it.

### DoD  
1. The proposal must include (but not limited to) following items  
- [ ] problem statement  
- [ ] objectives  
- [ ] proposed approach  
- [ ] data  
- [ ] measurement  
- [ ] expected results  
- [ ] required resources  
- [ ] system architecture  
- [ ] milestone/schedule  
- [ ] risk and concern

![[Cambrian-α_competition_2021_SOP_v3 (1).pdf]]
![]