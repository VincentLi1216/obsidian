<%*
const pre_path = "03 集美/b. 會議記錄/"
var file_name = "一般幹部會議"
var conf_num = 1;

async function checkFileExistence() {
  console.log("checking for the existence of "+pre_path+file_name+" "+String(conf_num));
  const fileExists = await tp.file.exists(pre_path+file_name+" "+String(conf_num));

  if (fileExists) {
    console.log("nice");
    console.log("The file "+file_name+" "+String(conf_num)+" existed!");
    conf_num += 1;
    return true
  }else {
	
    return false;
    // 对不存在文件的情况进行操作
  }
}

async function main() {
  while (await checkFileExistence()) {
    if(conf_num > 100){
      break;
    }
    
  }
  console.log(pre_path+file_name+" "+String(conf_num));
  //await tp.file.move(file_name);
  //tp.file.rename("一般幹部會議 " + String(conf_num));
  
}

const is_normal_meeting = await tp.system.suggester(["Yes", "No"], ["true", "false"],undefined, "Do you want to use this template as \"一般會議\" ?");

if(is_normal_meeting == "true"){
  console.log("yes, it is")
  main();
}else{
  console.log("no, it's not");
}



-%>