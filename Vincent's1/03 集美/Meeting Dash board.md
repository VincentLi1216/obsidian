# 一般幹部會議
```dataview
TABLE date AS Date, outline AS Outline FROM "03 集美"
WHERE type = "meeting" and parent = "一般幹部會議"
SORT date DESC
```

```dataview
CALENDAR date from "03 集美"
WHERE date != null and type = "meeting" and parent = "一般幹部會議"
```


# 會前會
```dataview
TABLE outline AS Outline FROM "03 集美"
WHERE type = "meeting" and parent = "會前會"
SORT date DESC
```

```dataview
CALENDAR date from "03 集美"
WHERE date != null and type = "meeting" and parent = "會前會"
```


# 活動會議
```dataview
TABLE parent AS Parent, outline AS Outline FROM "03 集美"
WHERE type = "meeting" and parent != "一般幹部會議" and parent != "會前會"
SORT date
```

```dataview
CALENDAR date from "03 集美"
WHERE date != null and type = "meeting" and parent != "一般幹部會議" and parent != "會前會"
```