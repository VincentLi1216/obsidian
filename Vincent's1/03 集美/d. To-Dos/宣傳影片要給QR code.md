---
type: todo
name: 宣傳影片要給QR code@23-07-16
state: done
date_created: 2023-07-16
parent: 新生營
due_date: 2023-07-17
assign_to: |
 - 李善得
 - 卓毓謙
---
# 新生營: 宣傳影片要給QR code

## Date Created
2023-07-16 Sun


## Assign to
- 李善得
- 卓毓謙


## Dead Line
2023-07-17


## Link
[[一般幹部會議 1#^e0b863| 宣傳影片要給QR code]]

### Note & Solution
[[影片QR Code]]
