# To-Dos
```dataview
TABLE parent AS Parent, assign_to AS "Assign To", due_date AS "DEAD LINE", due_date - date(today) as "Days Left" FROM "03 集美"
WHERE state = "todo"
SORT due_date - date(today) ASC
```

```dataview
CALENDAR due_date FROM "03 集美"
WHERE due_date != null and state = "todo"
```



# Dones
```dataview
TABLE parent AS Parent, assign_to as "Assign To", due_date as "DEAD LINE" FROM "03 集美"
WHERE type = "todo" and state = "done"
SORT due_date - date(today) ASC
```

```dataview
CALENDAR due_date FROM "03 集美"
WHERE due_date != null and state = "done"
```