# Installation
upgrade pip
```shell
pip3 install --upgrade pip
pip3 list
```

install virtualenv
```shell
pip3 install virtualenv
pip3 list
```

# Create virtualenv
```shell
virtualenv <env-name>
```

# Activate virtualenv
if the virtualenv name is "venv"
```shell
source venv/bin/activate
```

# Deactivate virtualenv
```shell
deactivate
```

# share/ install packages
share this dir's requirements
```shell
pip freeze > requirements.txt
```

download from requirements.txt
```shell
pip install -r requirements.txt
```


