
```python
import json  
  
with open('data.json') as f:  
	data = json.load(f)  
  
print(type(data))  
print(data)  
print(data['name'])  
print(data['age'])
```

```python
import json

# Python 的 dict 類型資料
myDict = {
    "name": "Jason",
    "age": 21,
    "skill": ["Python", "C/C++"],
    "married": False
}

# 將 Python 資料轉為 JSON 格式，儲存至 output.json 檔案
with open("output.json", "w") as f:
    json.dump(myDict, f, indent = 4)
```