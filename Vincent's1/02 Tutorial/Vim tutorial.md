### [[vimrc | .vimrc]]

### 取代
```vim
:[range]s/pattern/replacement/[flags]
```

- `[range]`：選擇要搜尋和取代的行範圍。例如，`1,10` 表示從第 1 行到第 10 行，`%` 表示全部行，`.,.+5` 表示從目前游標位置向下取 5 行，等等。如果省略 `[range]`，預設為目前行。
- `pattern`：要搜尋的模式（正規表示式）。
- `replacement`：取代搜尋到的模式。
- `flags`：可選的旗標，用於控制取代的行為。例如 `g` 表示全域替換（替換一行中所有符合的地方），`c` 表示取代前請確認每個匹配，`i` 表示忽略大小寫等等。如果省略 `flags`，預設只取代每行的第一個匹配。

以下是一些示例：

1. 在目前行取代第一個 "old" 為 "new"：

```vim
:s/old/new/
```

2. 在整個文件中取代所有 "old" 為 "new"：

```vim
:%s/old/new/g
```

3. 只在第 10 行到 20 行之間取代所有 "old" 為 "new"：

```vim
:10,20s/old/new/g
```

4. 全域替換，並在取代前確認每個匹配：

```vim
:%s/old/new/gc
```

### tabs
開啟新的tab
```vim
:tabe
```

開啟新的tab並開啟檔案
```vim
:tabe <filename>
```

切換tab
```vim
# to the right tab
:gt

# to the left tab
:gT
```

### windows
開啟新視窗(水平分割)
```vim
:new
```

開啟新視窗(垂直分割)
```vim
:vnew
```

切換視窗
```vim
control + w + h/j/k/l
```

### 一次就開啟多個檔案
```vim
# 水平開啟檔案
vim -o <file1> <file2>

# 垂直開啟檔案
vim -O <file1> <file2>

# 使用分頁視窗開啟檔案
vim -p
```

### 翻頁
```vim
# 向下翻頁
control + f

# 向上翻頁
control + b
```

### fold code
```vim
# 選取好範圍後，摺疊程式碼
zf

# 解開程式碼
zd

# 摺疊整段
zfip
```

### 重複之前動作
```vim
.
```

### 跳出與進入vim
```vim
# 跳出
control + z

# 進入
fg
```

### 立即啟用設定檔
```vim
:source ~/.vimrc
```

### 搜尋
```vim
/<search content>

# 取消高亮
:noh
```

### Open file
```vim
:e <filename>
```

### Change file name
```vim
:rename <oldname> <newname>
```

### mark
```vim
ma # 製作一個a的mark

'a #跳到a

:deletemarks#刪除mark
```

### 系統拷貝、貼上
```vim
# 拷貝
"*y 

#貼上
"*y
```

## Vim 外掛
### 使用Vim-plug當作外掛管理工具
[Vim-plug github link](https://github.com/junegunn/vim-plug)

下載vim-plug
```shell
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

在.vimrc中加入外掛的github連結
```vim
call plug#begin()

"在此加入想要的外掛
Plug 'folke/tokyonight.nvim'

call plug#end()
```

下載plug-ins
```vim
:PlugInstall
```

