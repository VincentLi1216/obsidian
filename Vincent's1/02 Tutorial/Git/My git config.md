
set vim as default editor
```shell
export GIT_EDITOR=nvim
git config --edit --global
```

git config
```.gitconfig

```
[user] 
  name = Vincent6_Li 
  email = Vincent6_Li@pegatroncorp.com
  
[checkout]
  defaultRemote = origin

[init] 
  default = branch default = branch default = branch defaultBranch = main

[alias]
  ac = !git add -A && git commit 
  co = checkout 
  lg = log —color —graph —pretty=format:’%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset’ —abbrev-commit 
 last = reset —hard HEAD^ 
 rs = reset —hard



