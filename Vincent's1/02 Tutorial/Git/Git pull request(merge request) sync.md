[github tutorial link](https://github.com/an920107/service-learning-website)

steps for sync up with the upstream(where the main project is holded):
```shell
git remote add upstream [My upstream URL] #
git remote -v # verify for adding upstream URL successfully
```

```shell
git fetch upstream
git checkout main
git merge upstream/main
```

or

```shell
git fetch upstream
git checkout Master
git merge upstream/Master
```

lift the restrictions of typing origin every time:
```shell
# choose either one
git config --edit
git config --edit --global
```

```shell
# add these two lines to set the default remote
[checkout]
	defaultRemote = origin
```