[youtube tutorial](https://www.youtube.com/watch?v=JWReY93Vl6g)
[his config file](https://github.com/NeuralNine/config-files/blob/master/init.vim)

在"~/.config/nvim/init.vim"加入：
```init.vim
" appearance setting
syntax enable
set t_co=256
filetype plugin on
"set background=dark
"colorscheme tokyonight-storm
syntax on
"colorscheme onedark
colorscheme desert
"line number
set number
set relativenumber
" enable mouse
set mouse=a
" enable ruler
set ruler
" search
set hlsearch
set ignorecase
set incsearch
set smartcase
" set cursor line
set cursorline
" disable swap file
set noswapfile
" set tab to 2 spaces
set softtabstop=2
set shiftwidth=2
" always show tab line
set showtabline=2
" set default split view below or right
set splitbelow
set splitright
"file type and indent
filetype on
filetype indent on
filetype plugin on
" enable line break
set linebreak
" show command
set showcmd
" scroll
set scrolloff=5
" maping
imap jj <Esc>
"plug-ins
call plug#begin()
"在此加入想要的外掛
"Plug 'folke/tokyonight.nvim'
Plug 'machakann/vim-sandwich'
Plug 'https://github.com/preservim/nerdtree' " NerdTree
Plug 'https://github.com/tpope/vim-commentary' " For Commenting gcc & gc
Plug 'https://github.com/vim-airline/vim-airline' " Status bar
Plug 'chrisbra/csv.vim'
Plug 'vim-airline/vim-airline-themes'
Plug 'https://github.com/rafi/awesome-vim-colorschemes' " Retro Scheme
Plug 'https://github.com/ryanoasis/vim-devicons' " Developer Icons
Plug 'https://github.com/tc50cal/vim-terminal' " Vim TerminalTerminal
Plug 'https://github.com/preservim/tagbar' " TagbarTagbar for code navigation
Plug 'https://github.com/terryma/vim-multiple-cursors' " CTRL + N for multiple cursors
Plug 'https://github.com/neoclide/coc.nvim'  " Auto Completion
Plug 'joshdick/onedark.vim' " onedark theme
call plug#end()
"nerdtree hotkey
"nnoremap <C-f> :NERDTreeFocus<CR>
nnoremap <C-n> :NERDTree<CR>
nnoremap <C-t> :NERDTreeToggle<CR>
" if icons don't show up correctly in nerdtree
"let g:NERDTreeDirArrowExpandable="+"
"let g:NERDTreeDirArrowCollapsible="~"
" run .py, .c, .cpp, .sh in vim
" nmap <F5> :call CompileRun()<CR>
" func! CompileRun()
"         exec "w"
" if &filetype == 'python'
"           exec "source virtualenv"
"             exec "!time python3 %"
" elseif &filetype == 'java'
"             exec "!javac %"
"             exec "!time java %<"
" elseif &filetype == 'cpp'
"                                                 exec "!runc %"
" elseif &filetype == 'c'
"                                                 exec "!runc %"
" elseif &filetype == 'sh'
"             :!time bash %
" endif
"     endfunc
"
" run .py, .c, .cpp, .sh in vim
nmap <F5> :call CompileRun()<CR>
" Define a function to return the path of the virtual environment if it exists
function! GetVenvPath()
    " get the current directory
    let l:current_dir = expand('%:p:h')
    let l:current_dir_name = fnamemodify(l:current_dir, ':t')
    let l:venv_path_dot = l:current_dir . '/.venv/bin/python3'
    let l:venv_path = l:current_dir . '/venv/bin/python3'
    let l:venv_path_alternative = l:current_dir . '/' . l:current_dir_name . '-virtualenv/bin/python3'
    if filereadable(l:venv_path_dot)
        return l:venv_path_dot
    elseif filereadable(l:venv_path)
        return l:venv_path
    elseif filereadable(l:venv_path_alternative)
        return l:venv_path_alternative
    else
        return 'python3'
    endif
endfunction
func! CompileRun()
    exec "w"
    if &filetype == 'python'
        " use the python interpreter from the virtual environment if it exists
        let l:python_interpreter = GetVenvPath()
        exec "!time " . l:python_interpreter . " %"
    elseif &filetype == 'java'
        exec "!javac %"
        exec "!time java %<"
	elseif &filetype == 'cpp'
        exec "!time runc %"
    elseif &filetype == 'c'
        exec "!time runc %"
    elseif &filetype == 'sh'
        :!time bash %
    endif
endfunc
" 在插入模式中用 Tab 键在补全项中前进
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
" 在插入模式中用 Shift-Tab 键在补全项中后退
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
" 在插入模式中用 Tab 键触发补全
inoremap <silent><expr> <C-a> coc#refresh()
" open nerdtree when it starts
autocmd VimEnter * NERDTree
let g:NERDTreeShowBookmarks=1
autocmd VimEnter * wincmd 1

command! Tcp call NcpFunc()
function! NcpFunc()
    let filepath = expand('%:p')
    execute '!cat ' . filepath
endfunction
```