```.gitlab-ci.yml
stages:
	- testing
	- building_image

image: python:3.8-alpine3.17

main-tests:
  stage: testing
  tags:
    - cambrianmodel-musculus01
  script:
    - echo "running tdd_example.py"
    - python3 --version
    - python --version
    - python3 test_calc.py
    - echo "Done running tdd_example.py"


```