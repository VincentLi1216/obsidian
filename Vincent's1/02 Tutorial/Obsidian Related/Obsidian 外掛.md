[Obsidian新手教程Ⅰ：8個用法完全解析，一站式入門丨雙向鏈接筆記軟件 | Full Course for Beginners【二一的筆記】
](https://www.youtube.com/watch?v=XeJ21baSV2g&t=349s)
## Appearance
- [ ] Tokyo Night

## Vim

- [ ] enable Vim in the setting -> editor -> advance
- [ ] relative line number
- [ ] vimrc

## 功能增強
- [ ] Annotator: For annotating pdfs
- [ ] Media Extended
- [ ] Obsidian Charts
- [ ] quick add: 快速新增文件
- [ ] commander: 在任何面板中加入按鈕，並且可以指定要執行什麼指令
- [ ] advanced tables: 快速在MD環境中修改表格
- [ ] Pandoc
- [ ] Obsidian Things3 Sync

## 面板增強
- [ ] Recent Files
- [ ] Calendar
- [ ] icon folder
- [ ] File Explorer Note Count

[[00 Templates/new]]
1. Djdj
2. Snsks
[[test_file]]
[[test_file]]

[a_normal_link](obsidian://open?vault=Documents&file=[[[[[[[[[[test]]]]]]]]]]_file)

```shell
echo hihi
```


```Python
print("Hello world ")
```

| 日期   | time | people | IDK | 甘特圖 |
| ------ | ---- |:------ | --- | ------ |
| su3cl3 |      |        |     |        |
|        |jeof|        |     |        |
|        |      |    wjeofe    |     |        |
|        |      |        |   awef  |   awefewfe    |
