---
variable: self_defined_variable
attendee: |
 - Vincent
 - YV
---

# [[<% tp.file.title %>]]

<% tp.date.now("YYYY-MM-DD, ddd") %>
<% tp.file.title %>
<% tp.frontmatter.variable %>
- <% tp.frontmatter.attendee %>

move the file into correct folder
<% await tp.file.move("/02 集美/d. Ideas & To-Dos/" + tp.file.title) %>

rename the file
<% tp.file.rename("prefix - " + tp.date.now()) %>