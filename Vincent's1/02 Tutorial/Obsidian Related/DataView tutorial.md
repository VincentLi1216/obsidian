# Ways to create meta data
type:: person
---
type: person
---

# Support types
- List
- Table
- Task
- Calendar

# Syntax(open edit mode to see them)
## Create TABLE
```dataview
TABLE project, date FROM "path"
```

```dataview
TABLE project FROM #todo 
```

## Create LIST
```dataview
LIST project FROM "path"
```

## Create TASK
```dataview
TASK project FROM "path"
WHERE !completed
```

## Create CALENDAR
```dataview
CALENDAR file.mtime 
```
tips: mtime means file's modify time

## Where
```dataview
TABLE project FROM "path"
WHERE project = "project X"
```

```dataview
TABLE project FROM "path"
WHERE contains(summary, "Alice")
```

## Sort
```dataview
TABLE project
WHERE type = "person"
SORT ASC
```

## As
```dataview
TABLE project, date as "project date "FROM "path"
```