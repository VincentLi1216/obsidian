### 讀取圖片
```python
import numpy as np
import cv2

# 讀取圖檔
img = cv2.imread('image.jpg')

# get the shape of the image
print(img.shape)

# 顯示圖片
cv2.imshow('My Image', img)

# 按下任意鍵則關閉所有視窗
cv2.waitKey(0)
cv2.destroyAllWindows()
```

### 寫入圖片
```python
# 寫入不同圖檔格式
cv2.imwrite('output.png', img)
cv2.imwrite('output.tiff', img)
```