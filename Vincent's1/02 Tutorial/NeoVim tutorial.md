# Installation
## NeoVim Installation
[github link](https://github.com/neovim/neovim/wiki/Installing-Neovim)
```shell
curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim.appimage
chmod u+x nvim.appimage
./nvim.appimage
```

If the `./nvim.appimage` command fails, try:
```shell
./nvim.appimage --appimage-extract
./squashfs-root/AppRun --version

# Optional: exposing nvim globally.
sudo mv squashfs-root /
sudo ln -s /squashfs-root/AppRun /usr/bin/nvim
nvim
```
## init.vim
### [[init-vim | Neo Vim init.vim]]

## Plugins
### download vim-plug
install vim-plug for all the plugins
```shell
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
```

### coc(auto complete) installation
安裝nodejs, npm(**linux**)Y
```shell
apt-get update
apt-get install nodejs
apt-get install npm
```

安裝nodejs, npm(**mac**)
```shell
brew install node
node -v
npm -v
```

安裝yarn
```shell
npm install -g yarn
```

查看nvim/plugged位置
```vim
:scriptnames
```

到nvim/plugged/coc.nvim底下執行
```shell
yarn install
yarn build
```

在nvim中打以下指令（需要用到哪些語言的auto complete就要安裝哪些）
```nvim
:CocInstall coc-python
```

假設還是遇到問題
```vim
:call coc#util#install():
```

### 安裝Nerdfont
```shell
# 創建一個用於存放字體的目錄
mkdir -p ~/.local/share/fonts/NerdFonts

# 進入該目錄
cd ~/.local/share/fonts/NerdFonts

# 下載 FiraCode Nerd Font
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/FiraCode.zip

# 解壓縮下載的字體檔案nvim
unzip FiraCode.zip

# 刪除原始的 zip 檔案
rm FiraCode.zip

# 更新字體快取
fc-cache -fv

```

# Tips
### vim-sandwich
```vim
# add surrounding
saiw( # 把字加上()
sa # visual mode選取字元後加上()

# delete surrounding
sd(

# change surrounding
sc([
```
