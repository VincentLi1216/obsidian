```python
import unittest
import calc
class TestCalc(unittest.TestCase):
  def test_add(self):
    self.assertEqual(calc.add(10,5), 15)
    self.assertEqual(calc.add(-1,1), 0)
    self.assertEqual(calc.add(-1,-1), -2)
  def test_subtract(self):
    self.assertEqual(calc.subtract(10,5), 5)
    self.assertEqual(calc.subtract(-1,1), -2)
    self.assertEqual(calc.subtract(-1,-1), 0)
  def test_multiply(self):
    self.assertEqual(calc.multiply(10,5), 50)
    self.assertEqual(calc.multiply(-1,1), -1)
    self.assertEqual(calc.multiply(-1,-1), 1)
  def test_divide(self):
    self.assertEqual(calc.divide(10,5), 2)
    self.assertEqual(calc.divide(-1,1), -1)
    self.assertEqual(calc.divide(-1,-1), 1)
    print(calc.divide(-5, -2))
    self.assertEqual(calc.divide(-5, -2), -5/-2)
    self.assertRaises(ValueError, calc.divide, 10, 0)
    with self.assertRaises(ValueError):
	    calc.divide(10, 0)
  
  def test_my_calc(self):
    #normal cases
    self.assertEqual(calc.my_calc(100, 33, "+"), 133)
    self.assertEqual(calc.my_calc(345, 100, "-"), 245)
    self.assertEqual(calc.my_calc(19, 7, "*"), 133)
    self.assertEqual(calc.my_calc(144, 12, "/"), 12)
    #with float
    self.assertEqual(calc.my_calc(100.123, 33, "+"), 133.123)
    self.assertEqual(calc.my_calc(345, 100.4678, "-"), 244.5322)
    self.assertEqual(calc.my_calc(19.009, 7, "*"), 133.063)
    self.assertEqual(calc.my_calc(144, 11.11, "/"), 12.961296129612961)
    #bad casese
    self.assertEqual(calc.my_calc(100.123, 33, "plus"), None)
    self.assertEqual(calc.my_calc(345, "參點壹肆壹伍玖貳陸", "-"), None)
    self.assertEqual(calc.my_calc(19.009, 0, "/"), None)
    self.assertEqual(calc.my_calc(0, "/"), None)
    self.assertEqual(calc.my_calc(), None)
    
    
       
if __name__ == "__main__":
  unittest.main()
```