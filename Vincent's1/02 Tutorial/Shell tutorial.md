#Tutorial 
# Tips
### 簡寫
```shell
# ls -al簡寫
ll
```

### 更改名稱
```shell
# change folder name
mv old_folder new_folder

# change file name
mv old_file.txt new_file.txt
```

### 複製資料夾
```shell
cp -r /path/to/source_folder /path/to/destination_folder
```

顯示進度
```shell
cp -r -v /path/to/source_folder /path/to/destination_folder
```

# zsh美化
[終端機美化原文](https://zhuanlan.zhihu.com/p/460785450)

### 安裝zsh
```shell
apt-get update
apt-get install zsh
```

### 查看可運行環境
```shell
cat /etc/shells
```

### 安裝 oh My Zsh
```shell
curl -L https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh | sh 
```

### 設zsh為預設shell
```shell
/root/.oh-my-zsh
```

若以上方法不行則手動修改 `~/.bashrc` 或 `~/.bash_profile` 檔案，再最後一行加入
```bash
exec zsh
```

### 設定喜歡的外觀
到用戶目錄底下的.zshrc更改ZSH_THEME
```shell
ZSH_THEME="jonathan"
```

### 加入Highlight&Auto Complete套件
```shell
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
```

```shell
cd ~/.oh-my-zsh/custom/plugins/
git clone https://github.com/zsh-users/zsh-autosuggestions
```

然後在.zshrc中加入
```shell
plugins=(   
  git  
  # other plugins...  
  zsh-autosuggestions  
  zsh-syntax-highlighting  
)
```

### 快捷鍵
在.zshrc中加入
```shell
alias la='ls -al'
```

### 重啟Shell
```shell
exec /bin/bash
```