# 補充
chopsticks 筷子
a pair of chopsticks 一雙筷子

jeans/paints/glasses 固定使用複數

There's / There are
There is a bold man standing there. And he's reading on his phone.
A bold man (who is) there is reading on his phone.

使用 how many時，預設結果為複數，故句子形式如下：
How many 複數名詞 are there

Taipei city hall 台北市政府
mall 商場

I love my families 我愛我家中的每個成員
I love my family 我愛我的家族

警察到場了
Police has / Polices have arrived. -> 習慣上使用複數

/現在式/過去式/過去分詞

你有跟幾個小孩子玩過?
How many kids have you played with?
我跟三個孩子玩過
I have played with 3 kids.

你經過幾家7-11？
How many 7-11s have you passed by?
I have passed by 7 7-11s.

#todo 在哪些時候ed要念"t"

24-7 = all the time = 24小時

# Vocabs
rent 房租
fuel 燃料
cost 花費
fare 費用

snack 零食（短母音）
snake 蛇（長母音）

chips 洋芋片/晶片
nuts 堅果/發瘋
You drive me crazy/nuts. 你讓我發瘋了

fridge = refrigerator 冰箱
TV = television 電視

flavor 口味

convenience(noun.) store 便利商店

Cost每件事的成本
Fee一個服務的費用
Fare交通工具的票錢
Charge小型服務的收費

# Exercise
1. 你有幾台手機?
	- How many phones do you have?
2. 你有多喜歡手機?
	- How much do you like this phone?
3. 你擁有過幾支手機?
	- How many phones have you had?
4. 你的手機多少錢?
	- How much is **this** phone?
5. 你的家裡有多少支手機?
	- How many phones are there in the family?

定慣詞(複數就不用加)
this/the/a



