### 補充
What's up? = 隨性版本的How are you

I am happy. 
I am in a good mood. (優)

recently = lately = 最近

一段時間沒有見面的朋友、熟人會使用：
How have you been?
意指在現在與上次見面期間過得好嗎

第一次見面：
How are you?
It's nice to meet you.

photographer 攝影師

### 單字
mood(moon??) 心情
Keep ones figure = 保持身材
figure = 數字（體重）

stress out = 壓力大到瘋掉

deal with 處理
你怎麼處理那個難搞的小孩
How do you deal with that impossible kid.

food court = 食物廣場（美食街）
單純講court認定為法院

I've been drunk a lot lately.
單獨講drinking的話，就會被認定為喝酒

pretty （good） = 蠻怎麼樣的

venue = 場地

### Use "How did you to make sentences"
1. How did you learn football?
2. How did you get to learn about drones?
	- search google and watch youTube videos
3. How did you get in that high school? 
	- I took the test


### The formula of talking to people
1. How are you today?
	- Not bad actually. How about you?
	- I am good.
2. How has your mood been recently?
	- My dog passed away, so not that good.
	- Why did he die.
3. How do you relieve stress?
	- I go on a jog whenever I can.
4. How did you get this job?
	- I put on cosmetics well.
5. How do I call the police?
	- Get a phone first.



### 語句
special = 某人事物是特別的
especially = 某人事物特別的怎麼樣(尤其是)
These gifts are special, especially the one with a red box.

I get（am) touched by people/strangers（陌生人） a lot, so I don't like to go to crowed places.

主詞 + 動詞 + 受詞 -> 完整句子