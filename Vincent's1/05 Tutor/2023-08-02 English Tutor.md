# Vocabs
- dress n. 洋裝
- dress v. 打扮/穿衣服
- dressing n. 穿搭

- greater 較大/更大
"2" is greater than "1"

- service 服務/保養
- arrange = plan 安排

- ceremony 儀式
e.g. graduation ceremony 畢典

- change v. 改變
- change n. 零錢

- dollar ＄錢

- purpose 目的/因素

- track 跟蹤/軌道
- step 腳步/步驟
- record 錄製/紀錄

- message 訊息
- messengers 通訊軟體

- mine 我的
Everyone has different phone cases, mine is blue.

- prefer 偏好
# 補充
### 有趣的last
later 更晚
latter 後者
A: Which one do you like, banana or orange?
B: I prefer latter.
latest 最新的
I have planned to buy the latest iPhone.

last adj. 最後
He was the last person to do the dishes.

last v. 堅持住
This building is designed to last at least 120 years.
Would our love last?

次數（聊到關於頻率會用到）
once = one time
twice = two times

第？次
1st
2nd
...
5th

- similar to 相似的
An electric car is similar to a regular car, but it is powered by batteries.

- desktop 桌上型電腦
- laptop 筆記型電腦
A desktop is similar to a laptop, but laptops can last longer when it's dark out(停電).

### The pronounce
- 一般時後
the car
the fish
the mouse

- 母音在後
the apple
the honest man
the interesting story

### the ship!?
ship是關於「關係」，有點像友誼的小船那樣

relation**ship** 愛情/友誼
scholar**ship** 獎學金

your 你的 you're 你


What I like the most about convenience stores is they sell game points.

### then vs than
- then 過去
	- I loved toy car **then**.
- than 比較
	- I loved my toy car more **than** the puzzle.

### I have to do sth
我必要做什麼事

### Sense 感官/感覺
很有幽默感
Peter has a sense of humor.

